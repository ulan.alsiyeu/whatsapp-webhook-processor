package errorhandler

import "gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"

const (
	rateLimitHitCode      = 130429
	temporaryBlockCode    = 368
	metaInternalErrorCode = 131000
)

// ErrorHandler реализация паттерна chain of responsibility
type ErrorHandler interface {
	handle(err *objects.Error)
	setNext(handler ErrorHandler)
}

func Handle(err *objects.Error) {
	metaErr := &MetaInternalError{}

	temporaryBlockErr := &TemporaryBlock{}
	temporaryBlockErr.setNext(metaErr)

	rateLimitErr := &RateLimitHit{}
	rateLimitErr.setNext(temporaryBlockErr)

	rateLimitErr.handle(err)
}
