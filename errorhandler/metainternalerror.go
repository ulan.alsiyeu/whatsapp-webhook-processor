package errorhandler

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

type MetaInternalError struct {
	next ErrorHandler
}

func (mie *MetaInternalError) handle(err *objects.Error) {
	if err.Code == metaInternalErrorCode {
		fmt.Printf("Error: %d. Message: %s. %s. %s", err.Code, err.Title, err.Message, err.ErrorData.Details)
		return
	}

	fmt.Printf("Unknown error with code: %d", err.Code)
}

func (mie *MetaInternalError) setNext(handler ErrorHandler) {
	mie.next = handler
}
