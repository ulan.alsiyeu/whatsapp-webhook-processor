package errorhandler

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

type RateLimitHit struct {
	next ErrorHandler
}

func (rlh *RateLimitHit) handle(err *objects.Error) {
	if err.Code == rateLimitHitCode {
		fmt.Printf("Error: %d. Message: %s. %s. %s", err.Code, err.Title, err.Message, err.ErrorData.Details)
		return
	}

	rlh.next.handle(err)
}

func (rlh *RateLimitHit) setNext(handler ErrorHandler) {
	rlh.next = handler
}
