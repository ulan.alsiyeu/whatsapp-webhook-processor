package errorhandler

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

type TemporaryBlock struct {
	next ErrorHandler
}

func (tb *TemporaryBlock) handle(err *objects.Error) {
	if err.Code == temporaryBlockCode {
		fmt.Printf("Error: %d. Message: %s. %s. %s", err.Code, err.Title, err.Message, err.ErrorData.Details)
		return
	}

	tb.next.handle(err)
}

func (tb *TemporaryBlock) setNext(handler ErrorHandler) {
	tb.next = handler
}
