package messagebuilder

import "gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"

func newTextMessageBuilder() *textMessageBuilder {
	return &textMessageBuilder{}
}

type textMessageBuilder struct {
	respondMessageID  string
	outgoingMessageID string
	userPhoneNumber   string
	messageType       string
	text              string
	createdAt         string
}

func (b *textMessageBuilder) setRespondMessageID(id string) {
	b.respondMessageID = id
}

func (b *textMessageBuilder) setOutgoingMessageID(id string) {
	b.outgoingMessageID = id
}

func (b *textMessageBuilder) setUserNumber(number string) {
	b.userPhoneNumber = number
}

func (b *textMessageBuilder) setType(messageType string) {
	b.messageType = messageType
}

func (b *textMessageBuilder) setText(text string) {
	b.text = text
}

func (b *textMessageBuilder) setCreatedAt(timestamp string) {
	b.createdAt = timestamp
}

func (b *textMessageBuilder) Build(messagePayload objects.Message) Message {
	b.setRespondMessageID(messagePayload.MessageID)
	b.setUserNumber(messagePayload.From)
	b.setType(messagePayload.Type)
	b.setText(messagePayload.Text.Body)
	b.setCreatedAt(messagePayload.Timestamp)

	if messagePayload.Context != nil {
		b.setOutgoingMessageID(messagePayload.Context.OutgoingMessageID)
	}

	return Message{
		RespondMessageID:  b.respondMessageID,
		UserPhoneNumber:   b.userPhoneNumber,
		Type:              b.messageType,
		Text:              b.text,
		OutgoingMessageID: b.outgoingMessageID,
		CreatedAt:         b.createdAt,
	}
}
