package messagebuilder

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

func newDocumentMessageBuilder() *documentMessageBuilder {
	return &documentMessageBuilder{}
}

type documentMessageBuilder struct {
	respondMessageID  string
	outgoingMessageID string
	userPhoneNumber   string
	messageType       string
	text              string
	createdAt         string
}

func (b *documentMessageBuilder) setRespondMessageID(id string) {
	b.respondMessageID = id
}

func (b *documentMessageBuilder) setOutgoingMessageID(id string) {
	b.outgoingMessageID = id
}

func (b *documentMessageBuilder) setUserNumber(number string) {
	b.userPhoneNumber = number
}

func (b *documentMessageBuilder) setType(messageType string) {
	b.messageType = messageType
}

func (b *documentMessageBuilder) setText(text string) {
	b.text = text
}

func (b *documentMessageBuilder) setCreatedAt(timestamp string) {
	b.createdAt = timestamp
}

func (b *documentMessageBuilder) Build(messagePayload objects.Message) Message {
	messageText := fmt.Sprintf("Document message is sent with id: %s", messagePayload.Image.ID)

	if messagePayload.Image.Caption != "" {
		messageText += fmt.Sprintf(" and with caption: %s", messagePayload.Document.Caption)
	}

	if messagePayload.Context != nil {
		b.setOutgoingMessageID(messagePayload.Context.OutgoingMessageID)
	}

	b.setRespondMessageID(messagePayload.MessageID)
	b.setUserNumber(messagePayload.From)
	b.setType(messagePayload.Type)
	b.setText(messageText)
	b.setCreatedAt(messagePayload.Timestamp)

	return Message{
		RespondMessageID:  b.respondMessageID,
		UserPhoneNumber:   b.userPhoneNumber,
		Type:              b.messageType,
		Text:              b.text,
		OutgoingMessageID: b.outgoingMessageID,
		CreatedAt:         b.createdAt,
	}
}
