package messagebuilder

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

func newAudioMessageBuilder() *audioMessageBuilder {
	return &audioMessageBuilder{}
}

type audioMessageBuilder struct {
	respondMessageID  string
	outgoingMessageID string
	userPhoneNumber   string
	messageType       string
	text              string
	createdAt         string
}

func (b *audioMessageBuilder) setRespondMessageID(id string) {
	b.respondMessageID = id
}

func (b *audioMessageBuilder) setOutgoingMessageID(id string) {
	b.outgoingMessageID = id
}

func (b *audioMessageBuilder) setUserNumber(number string) {
	b.userPhoneNumber = number
}

func (b *audioMessageBuilder) setType(messageType string) {
	b.messageType = messageType
}

func (b *audioMessageBuilder) setText(text string) {
	b.text = text
}

func (b *audioMessageBuilder) setCreatedAt(timestamp string) {
	b.createdAt = timestamp
}

func (b *audioMessageBuilder) Build(messagePayload objects.Message) Message {
	b.setRespondMessageID(messagePayload.MessageID)
	b.setUserNumber(messagePayload.From)
	b.setType(messagePayload.Type)
	b.setText(fmt.Sprintf("Audio message with id: %s", messagePayload.Audio.ID))
	b.setCreatedAt(messagePayload.Timestamp)

	if messagePayload.Context != nil {
		b.setOutgoingMessageID(messagePayload.Context.OutgoingMessageID)
	}

	return Message{
		RespondMessageID:  b.respondMessageID,
		UserPhoneNumber:   b.userPhoneNumber,
		Type:              b.messageType,
		Text:              b.text,
		OutgoingMessageID: b.outgoingMessageID,
		CreatedAt:         b.createdAt,
	}
}
