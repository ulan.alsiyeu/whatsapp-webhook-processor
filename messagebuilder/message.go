package messagebuilder

import "gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"

type MessageBuilder interface {
	SetMessageID(id int)
	SetUserNumber(number string)
	SetType(messageType string)
	SetText(text string)
	SetOutgoingMessageID(id string)
	SetCreatedAt(timestamp string)
}

func GetMessageBuilder(messageType string) MessageBuilder {
	switch messageType {
	case objects.MessageTypeText:
		return nil
	case objects.MessageTypeAudio:
		return nil
	case objects.MessageTypeImage:
		return nil
	case objects.MessageTypeDocument:
		return nil
	case objects.MessageTypeButton:
		return nil
	case objects.MessageTypeSticker:
		return nil
	case objects.MessageTypeVideo:
		return nil
	case objects.MessageTypeReaction:
		return nil
	default:
		return nil
	}
}
