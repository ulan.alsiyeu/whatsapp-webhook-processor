package messagebuilder

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

func newStickerMessageBuilder() *stickerMessageBuilder {
	return &stickerMessageBuilder{}
}

type stickerMessageBuilder struct {
	respondMessageID  string
	outgoingMessageID string
	userPhoneNumber   string
	messageType       string
	text              string
	createdAt         string
}

func (b *stickerMessageBuilder) setRespondMessageID(id string) {
	b.respondMessageID = id
}

func (b *stickerMessageBuilder) setOutgoingMessageID(id string) {
	b.outgoingMessageID = id
}

func (b *stickerMessageBuilder) setUserNumber(number string) {
	b.userPhoneNumber = number
}

func (b *stickerMessageBuilder) setType(messageType string) {
	b.messageType = messageType
}

func (b *stickerMessageBuilder) setText(text string) {
	b.text = text
}

func (b *stickerMessageBuilder) setCreatedAt(timestamp string) {
	b.createdAt = timestamp
}

func (b *stickerMessageBuilder) Build(messagePayload objects.Message) Message {
	messageText := fmt.Sprintf("Sticker is sent with id: %s. For message with id: %s", messagePayload.Sticker.ID, messagePayload.Context.OutgoingMessageID)

	b.setRespondMessageID(messagePayload.MessageID)
	b.setOutgoingMessageID(messagePayload.Context.OutgoingMessageID)
	b.setUserNumber(messagePayload.From)
	b.setType(messagePayload.Type)
	b.setText(messageText)
	b.setCreatedAt(messagePayload.Timestamp)

	return Message{
		RespondMessageID:  b.respondMessageID,
		UserPhoneNumber:   b.userPhoneNumber,
		Type:              b.messageType,
		Text:              b.text,
		OutgoingMessageID: b.outgoingMessageID,
		CreatedAt:         b.createdAt,
	}
}
