package messagebuilder

import (
	"fmt"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

func newImageMessageBuilder() *imageMessageBuilder {
	return &imageMessageBuilder{}
}

type imageMessageBuilder struct {
	respondMessageID  string
	outgoingMessageID string
	userPhoneNumber   string
	messageType       string
	text              string
	createdAt         string
}

func (b *imageMessageBuilder) setRespondMessageID(id string) {
	b.respondMessageID = id
}

func (b *imageMessageBuilder) setOutgoingMessageID(id string) {
	b.outgoingMessageID = id
}

func (b *imageMessageBuilder) setUserNumber(number string) {
	b.userPhoneNumber = number
}

func (b *imageMessageBuilder) setType(messageType string) {
	b.messageType = messageType
}

func (b *imageMessageBuilder) setText(text string) {
	b.text = text
}

func (b *imageMessageBuilder) setCreatedAt(timestamp string) {
	b.createdAt = timestamp
}

func (b *imageMessageBuilder) Build(messagePayload objects.Message) Message {
	messageText := fmt.Sprintf("Image message is sent with id: %s", messagePayload.Image.ID)

	if messagePayload.Image.Caption != "" {
		messageText += fmt.Sprintf(" and with caption: %s", messagePayload.Image.Caption)
	}

	if messagePayload.Context != nil {
		b.setOutgoingMessageID(messagePayload.Context.OutgoingMessageID)
	}

	b.setRespondMessageID(messagePayload.MessageID)
	b.setUserNumber(messagePayload.From)
	b.setType(messagePayload.Type)
	b.setText(messageText)
	b.setCreatedAt(messagePayload.Timestamp)

	return Message{
		RespondMessageID:  b.respondMessageID,
		UserPhoneNumber:   b.userPhoneNumber,
		Type:              b.messageType,
		Text:              b.text,
		OutgoingMessageID: b.outgoingMessageID,
		CreatedAt:         b.createdAt,
	}
}
