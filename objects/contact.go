package objects

// Contact ...
type Contact struct {
	WhatsappID string  `json:"wa_id"`
	Profile    Profile `json:"profile"`
}

type Profile struct {
	Name string `json:"name"`
}
