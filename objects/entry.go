package objects

const (
	errorType   = "error"
	messageType = "message"
	statusType  = "status"
)

type Entry struct {
	ID      string   `json:"id"`
	Changes []Change `json:"changes"`
}

type Change struct {
	Value Value  `json:"value"`
	Field string `json:"field"`
}
