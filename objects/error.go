package objects

// Error ...
type Error struct {
	Code      int       `json:"code"`
	Title     string    `json:"title"`
	Message   string    `json:"message"`
	ErrorData ErrorData `json:"error_data"`
}

type ErrorData struct {
	Details string `json:"details"`
}
