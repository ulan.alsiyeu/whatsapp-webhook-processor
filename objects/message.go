package objects

// Message ...
type Message struct {
	From        string       `json:"from"`
	MessageID   string       `json:"id"`
	Type        string       `json:"type"`
	Context     *Context     `json:"context"`
	Text        *Text        `json:"text"`
	Audio       *Audio       `json:"audio"`
	Image       *Image       `json:"image"`
	Video       *Video       `json:"video"`
	Button      *Button      `json:"button"`
	Document    *Document    `json:"document"`
	Sticker     *Sticker     `json:"sticker"`
	Reaction    *Reaction    `json:"reaction"`
	Interactive *Interactive `json:"interactive"`
	Referral    *Referral    `json:"referral"`
	Errors      []*Error     `json:"errors"`
	Timestamp   string       `json:"timestamp"`
}

type Context struct {
	Forwarded           bool   `json:"forwarded"`
	FrequentlyForwarded bool   `json:"frequently_forwarded"`
	From                string `json:"from"`
	OutgoingMessageID   string `json:"id"`
}

type Text struct {
	Body string `json:"body"`
}

type Audio struct {
	ID       string `json:"id"`
	MIMEType string `json:"mime_type"`
}

type Image struct {
	ID       string `json:"id"`
	Caption  string `json:"caption"`
	Hash     string `json:"sha256"`
	MIMEType string `json:"mime_type"`
}

type Video struct {
	ID       string `json:"id"`
	Caption  string `json:"caption"`
	FileName string `json:"file_name"`
	Hash     string `json:"sha256"`
	MIMEType string `json:"mime_type"`
}

type Button struct {
	Payload string `json:"objects"`
	Text    string `json:"text"`
}

type Document struct {
	ID       string `json:"id"`
	Caption  string `json:"caption"`
	FileName string `json:"file_name"`
	Hash     string `json:"sha256"`
	MIMEType string `json:"mime_type"`
}

type Sticker struct {
	ID       string `json:"id"`
	Hash     string `json:"sha256"`
	MIMEType string `json:"mime_type"`
}

type Reaction struct {
	ID    string `json:"message_id"`
	Emoji string `json:"emoji"`
}

type Interactive struct {
	Type InteractiveType `json:"type"`
}

type InteractiveType struct {
	ButtonReply ButtonReply `json:"button_reply"`
}

type ButtonReply struct {
	ID    string `json:"id"`
	Title string `json:"title"`
}

type Referral struct {
	SourceURL    string `json:"source_url"`
	SourceType   string `json:"source_type"`
	SourceID     string `json:"source_id"`
	Headline     string `json:"headline"`
	Body         string `json:"body"`
	MediaType    string `json:"media_type"`
	ImageURL     string `json:"image_url"`
	VideoURL     string `json:"video_url"`
	ThumbnailURL string `json:"thumbnail_url"`
}
