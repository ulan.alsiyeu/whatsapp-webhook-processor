package objects

// MetaData ...
type MetaData struct {
	PhoneNumber   string `json:"display_phone_number" binding:"required"`
	PhoneNumberID string `json:"phone_number_id" binding:"required"`
}
