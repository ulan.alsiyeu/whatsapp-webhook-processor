package objects

// Status ...
type Status struct {
	WAMessageID  string         `json:"id"`
	RecipientID  string         `json:"recipient_id"`
	Status       string         `json:"status"`
	Conversation Conversation   `json:"conversation"`
	Errors       []StatusErrors `json:"errors"`
	Pricing      Pricing        `json:"pricing"`
	Timestamp    string         `json:"timestamp"`
}

type Conversation struct {
	ID                  string `json:"id"`
	Origin              Origin `json:"origin"`
	ExpirationTimestamp string `json:"expiration_timestamp"`
}

type StatusErrors struct {
	Code  int    `json:"code"`
	Title string `json:"title"`
}

type Origin struct {
	Type string `json:"type"`
}

type Pricing struct {
	Category     string `json:"category"`
	PricingModel string `json:"pricing_model"`
}
