package objects

import (
	"errors"
)

var ErrInvalidPayloadType = errors.New("invalid payload type")

type Value struct {
	MessagingProduct string    `json:"messaging_product"`
	MetaData         MetaData  `json:"metadata" binding:"required"`
	Contacts         []Contact `json:"contacts"`
	Errors           []Error   `json:"errors"`
	Statuses         []Status  `json:"statuses"`
	Messages         []Message `json:"messages"`
}

func (v *Value) Type() (string, error) {
	switch {
	case v.Errors != nil:
		return errorType, nil
	case v.Messages != nil:
		return messageType, nil
	case v.Statuses != nil:
		return statusType, nil
	default:
		return "", ErrInvalidPayloadType
	}
}
