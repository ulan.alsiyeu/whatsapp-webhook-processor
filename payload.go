package wawebhook

import (
	"errors"
	"github.com/gin-gonic/gin"
	"gitlab.com/ulan.alsiyeu/whatsapp-webhook-processor/objects"
)

var ErrInvalidWebhookPayload = errors.New("invalid webhook payload")

// Payload ...
type Payload struct {
	Object  string          `json:"object"`
	Entries []objects.Entry `json:"entry"`
}

func GetPayloadFromRequest(ctx *gin.Context) (*Payload, error) {
	p := &Payload{}

	if err := ctx.ShouldBindJSON(&p); err != nil {
		return nil, ErrInvalidWebhookPayload
	}

	return p, nil
}

func GetChangesFromRequest(ctx *gin.Context) ([]objects.Change, error) {
	p, err := GetPayloadFromRequest(ctx)
	if err != nil {
		return nil, err
	}

	var changes []objects.Change
	for _, entry := range p.Entries {
		changes = append(changes, entry.Changes...)
	}

	return changes, err
}
